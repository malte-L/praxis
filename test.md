---
marp: true
math: katex

paginate: true
size: 16:9
---



<!-- 
einen Überblick über Ihr Praktikum und die dort bearbeiteten Projekte/Themen gibt,
mindestens zu 50% technisch in die Tiefe geht - ich möchte ein Gefühl dafür bekommen, wo die Herausforderungen und SChwierigkeiten lagen -,
sowohl ihre Erfahrungen und ihre Lernkurve reflektiert als auch den Aspekt betrachtet, wie gut Sie sich durch das Studium vorbereitet gefühlt haben, und
ggf. wie es mit Bachelorarbeit etc. weiter geht. 
 -->

# Praxisprojekt
19INB  6. Semester
Malte Lehmann 
RIB Software GmbH


***


# Gliederung
1. Betrieb
2. Tätigkeiten / Technologien
3. Projekte
4. Erfahrungen
5. Bachelorarbeit


***
# 1. Betrieb
![bg right w:400](assets/rib-logo.png)
- 1961 gegründet
- mehr als 30 Niederlassungen international
- Hauptsächlich ERP
- Fokus auf Bauunternehmen


***
# 1. Betrieb - Kunden
- Bauunternehmen / Architektur
- insgesamt um die 400 Kunden
![w:600](assets/icons.png)


***
# 2. Tätigkeiten / Technologien
- Team "Apps"
- Flutter Framework für Android/IOS
![w:200](assets/flutter-log.svg)
- Android Studio
- Git(-lab)
- Eigene ERP Software für Tickets / Urlaub / Zeiten


***
# 3. Projekte - Überblick
- geplant:
  - 2 Wochen: In-App Kamera-Ansicht
  - restliche Zeit: Einbau von PDFTron
- Bug-Fixing / neue Features in schon bestehendem Code
- Push-Notifications
- GPS-Koordinaten in Exif-Meta-Daten schreiben
- Unterstützung von Kollegen
- Clustering

***
![bg h:700](assets/non-clustered.png)
![bg h:700](assets/clustererd-zoomed-out.png)
***


video
***
# Clustering


![bg w:500 right:40%](assets/simple-tree.svg)
```dart
class AbstractPin {
  Offset position;

  AbstractPin({required this.position});
}

class SinglePin extends AbstractPin {
  /* ... */

  SinglePin({required super.position});
}


class ClusterPin extends AbstractPin {
  final List<AbstractPin> children;
  late final int count = _calcCount(children);

  ClusterPin({required super.position, required this.children});
}

int _calcCount(List<AbstractPin> pins) {
  var sum = 0;
  for (final p in pins) {
    sum += p is SinglePin ? 1 : (p as ClusterPin).count;
  }
  return sum;
}
```

***

# Clustering - Eine Schicht



```dart
List<AbstractPin> cluster(List<AbstractPin> pins, {final int n = 1, required final int maxDistance}) {
  final Map<AbstractPin, bool> alreadyVisited = {};
  final List<AbstractPin> ret = [];

  for (final p in pins) {
    if (alreadyVisited[p] ?? false) continue;
    alreadyVisited[p] = true;

    final nearestNeighbours = <AbstractPin>[];
    for (var i = 0; i < n; i++) {
      final nearest = findNearest(pins, p, alreadyVisited, maxDistance);
      if (nearest == null) break; // no more pins found that are close enough and not already visited
      alreadyVisited[nearest] = true;
      nearestNeighbours.add(nearest);
    }

    if (nearestNeighbours.isEmpty) {
      ret.add(p);
      continue;
    }

    final newChildren = <AbstractPin>[p];
    var center = p.position;

    for (final nearest in nearestNeighbours) {
      center = center.middlePoint(nearest.position);
      newChildren.add(nearest);
    }

    final clusterPin = ClusterPin(position: center, children: newChildren.toList(growable: false));
    ret.add(clusterPin);
  }
  return ret;
}
```

***


![bg 90%](assets/0-1-schritt.svg)



***

# Clustering - Mehrere Schichten
```dart
typedef ClusterLayer = List<AbstractPin>;

class ClusterDepthStorage {
  final _internal = <ClusterLayer>[];

  ClusterDepthStorage();

  factory ClusterDepthStorage.fromList(
    List<AbstractPin> pins, {
    final int depth = TileViewConstants.clusterTreeMaxDepth,
    final int n = TileViewConstants.clusterMaxChildsForClusterPin,
    int maxDistance = TileViewConstants.clusterMaxDistance,
  }) {
    assert(depth >= 1 && maxDistance > 0 && n > 0);
    final ret = ClusterDepthStorage();

    final dStep = (maxDistance / depth).floor();
    maxDistance = dStep;

    ret._internal.add(pins); // most inner layer is just the non-clustered set of pins

    for (var i = 1; i < depth; i++) {
      ret._internal.add(cluster(ret._internal[i - 1], n: n, d: maxDistance));
      maxDistance += dStep;
    }
    return ret;
  }

  ClusterLayer layerForDepth(int depth) {
    return _internal[max(0, min(_internal.length - 1, depth))];
  }
}
```

***
![bg 90%](assets/1-2-schritt.svg)

***

![bg 90%](assets/schritte.svg)

***



```dart
const clusterDepthForZoomLevelMapping = <C2<double, int>>[
  C2(10.0, clusterNoClusteringDepth),
  C2(3.0, 1),
  C2(1.5, 2),
  C2(0.7, 3),
  C2(0.5, 4),
  C2(0.3, 5),
  C2(0.0, clusterTreeMaxDepth),
];
  ```


```dart
int clusterDepthForZoomLevel(double zoomLevel) {
  for (final zoomLevels in clusterDepthForZoomLevelMapping) {
    if (zoomLevel >= zoomLevels.first) return zoomLevels.second;
  }
  // should never go here
  return clusterDepthForZoomLevelMapping.last.second;
}
```
***






## Vielen Dank für ihre Aufmerksamkeit